import { Component, OnInit } from '@angular/core';
import Student from '../../entity/student';
import { StudentService } from '../../service/student-service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})
export class StudentsAddComponent implements OnInit{
  model: Student = new Student();
  fb: FormBuilder = new FormBuilder();
  sf: FormGroup;
  validation_messages = {
    'studentId':[
      { type: 'required', message: 'student id is required'},
      { type:'pattern', message: 'student id must be 10 digits'},
    ],
    'name':[
      {
        type: 'required', message: 'the name is required'
      }
    ],
    'surname': [
      {
        type: 'required', message: 'the surname is required'
      }
    ],
    'penAmount':[
      {type: 'pattern', message: 'the penamout must be a number'}
    ],
    'image': [],
    'description': []
  };
  constructor(private studentService: StudentService, private router: Router) { }
  ngOnInit(): void {
    this.sf = this.fb.group({
      studentId: [null, Validators.compose([Validators.required,Validators.pattern('[0-9]{10}')])],
      name: [null, Validators.required],
      surname: [null, Validators.required],
      penAmount: [null, Validators.compose([Validators.required, Validators.pattern('[0-9]+')])],
      image: [null],
      description: [null]
    });
  }
  upQuantity(student: Student) {
    student.penAmount++;
  }

  downQuantity(student: Student) {
    if (student.penAmount > 0) {
      student.penAmount--;
    }
  }
  onSubmit() {
    this.model = this.sf.value;
    this.studentService.saveStudent(this.model)
      .subscribe((student) => {
        this.router.navigate(['/detail', student.id]);
      }, (error) => {
        alert('could not save value');
      });
  }
  // TODO: Remove this when we're done
  get diagnostic() { return JSON.stringify(this.model); }

}
